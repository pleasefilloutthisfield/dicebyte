const T = 9;
game = {
    gridCount: 2,
    tutor:true,
    score: {
        enemy: 0,
        enemyLast: 0,
        player: 0,
        playerLast: 0,
    },
    S: {
        0: "",
        1: "black",
        2: "red",
        3: "green",
        4: "bit",
        5: "diamond"
    },
    ais: {
        0: { id: "Beginner Bot", S: 0, R: -1, },
        1: { id: "Value Bot", S: 3 },
        2: { id: "Killer Bot", S: 2 },
        3: { id: "Danger Bot", S: 1 },
        4: { id: "Champion Bot", S: 5, R: 1 }
    },
    achievements: {
        0: { n: "Victorious", t: "Win a game", l: 0 },
        1: { n: "Fabulous", t: "Win a game with a non default dice skin", l: 0 },
        2: { n: "Glorious", t: "Complete all levels", l: 0 },
        3: { n: "Only Aces", t: "Roll three 6's in a row", l: 0 },
        4: { n: "Highlander", t: "Fill a column with 1's", l: 0 },
        5: { n: "Number of the beast", t: "Fill a column with 6's", l: 0 },
        6: { n: "Fast game", t: "Win a game in 10 or less turns", l: 0 },
        7: { n: "Slow game", t: "Win a game in 30 or more turns", l: 0 },
        8: { n: "Suprise Win", t: "End a game early with a sequence", l: 0 },
        9: { n: "Nope", t: "Remove your opponents dice five turns in a row.", l: 0 },
        10: { n: "Boom", t: "Remove 3 dice at once", l: 0 },
        11: { n: "Feeling Blue", t: "Win while you opponents has an empty grid", l: 0 },
        12: { n: "Why so serious?", t: "Roll the joker dice three times in a match", l: 0 },
        13: { n: "Overachiever", t: "Have 50 points more than you opponent", l: 0 },
        14: { n: "Perfection", t: "Complete all other achievements", l: 0 },
    },
    grids: {},
    current: {
        roll: 0,
        turn: 0,
        player: 1,
        hs: 0,
        ws: 0,
        rc: 0,
        S: 0,
        J: 1,
        EJ: 1,
    },
    controls: {
        roll: "KeyW",
        1: "KeyA",
        2: "KeyS",
        3: "KeyD",
    },
    ani: {
        dice: 300,
        def: 1000,
        ng: 5000,
        aiTurnSpeed: 200,
    }
};

function rand(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max) + 1;
    return Math.floor(Math.random() * (max - min)) + min;
}
function getIntFromString(string) {
    return string.replace(/[^\d.]/g, '');
}
function tutorial(condition, cb){
    const gameTurn = game.current.turn - [];
    setTimeout(()=>{
        if(game.tutor && condition && gameTurn === game.current.turn){
            cb();
        }
    }, game.ani.ng)

}
function buildGrid() {
    let gridObj = {
        0: { 0: {}, 1: {}, 2: {} },
        1: { 0: {}, 1: {}, 2: {} },
        2: { 0: {}, 1: {}, 2: {} }
    };
    const root = document.createElement("div");
    root.className = "grid";

    for (let i = 0; i < 3; i++) {
        let row = document.createElement("div");
        row.className = "row";
        row.setAttribute("row", i);
        for (let ii = 0; ii < 3; ii++) {
            let spot = document.createElement("div");
            spot.className = "spot";
            spot.setAttribute("spot", ii);
            spot.addEventListener("click", () => {
                spotclick(spot);
            });
            row.appendChild(spot);
        }
        root.appendChild(row);
    }
    return { root: root, gridObj: gridObj }
}
function manElm(elm, options) {
    let returnNewElm = false;
    if (!elm) {
        elm = document.createElement("div");
        returnNewElm = true;
    }
    if (elm && options) {
        if (options.inner || options.wipe) {
            elm.innerHTML = "";
        }
        if (options.inner) {
            elm.innerHTML = options.inner;
        }
        if (options.append) {
            elm.appendChild(options.append)
        }
        if (options.addClasss) {
            elm.classList.add(options.addClasss)
        }
        if (options.removeClass) {
            elm.classList.remove(options.removeClass)
        }
    }
    if (returnNewElm) {
        return elm;
    }
}
function roll(joker, fixed) {

    if (game.rollDisabled && !joker) { return; }

    if (joker && game.current.J > 0 && game.current.player === 1) { game.current.J -= 1; game.jokerRoll = true; }
    else if (joker && game.current.EJ > 0 && game.current.player === 0) { game.current.EJ -= 1; game.jokerRoll = true; }
    else if (joker && game.current.J < 1 || joker && game.current.EJ < 1) { return; }
    manElm(document.getElementById("roll"), { addClasss: "disabled", removeClass: "clickRoll" });
    game.current.dicePlaced = false;

    game.current.roll = rand(1, 6);
    if (fixed) { game.current.roll = fixed; }
    const dice = document.createElement("d" + game.current.roll);
    appendDots(dice, game.current.roll);
    const cloneDice = dice.cloneNode(true);
    if (game.current.player === 1) {
        if (game.lastPlayerRoll === 6 && game.current.playerRoll === 6 && game.current.roll === 6) {
            claimAchievement(3);
        }
        game.lastPlayerRoll = game.current.playerRoll;
        game.current.playerRoll = game.current.roll;
        document.getElementById("roll").classList.add("disabled");
        document.getElementById("specialRoll").classList.add("ninja");
        if (!game.current.dicePlaced && !joker) {

            manElm(document.getElementById("lastRoll"), { removeClass: "joker" });

            setTimeout(() => {
                if (game.current.J > 0 && !game.current.dicePlaced && game.rollDisabled === true) {
                    document.getElementById("specialRoll").classList.remove("ninja");
                }

            }, game.ani.def * 2);

        }
        else if (joker && !game.current.dicePlaced) {
            manElm(document.getElementById("lastRoll"), { removeClass: "animateDice", addClasss: "joker" });
        }
        if(game.tutor){
            tutorial(!game.current.dicePlaced && game.current.player, ()=>{
                manElm(document.getElementById("grid1"), { addClasss: "placeDice" });
            });
        }



        document.getElementById("lastRoll").innerHTML = "";
        setTimeout(() => {
            manElm(document.getElementById("lastRoll"), { wipe: true, append: cloneDice, removeClass: "filterGray", addClasss: "animateDice" },);
        })

    }
    else {
        if (joker && !game.current.dicePlaced) {
            manElm(document.getElementById("lastEnemyRoll"), { removeClass: "animateDice", addClasss: "joker" });
        }
        else {
            manElm(document.getElementById("lastEnemyRoll"), { removeClass: "joker" });
        }
        setTimeout(() => {
            manElm(document.getElementById("lastEnemyRoll"), { wipe: true, append: cloneDice, removeClass: "filterGray", addClasss: "animateDice" });
        })

    }
    game.rollDisabled = true;

}
function spotclick(spotElm) {
    if (game.current.player === 0) { return }
    const spotNumber = spotElm.getAttribute("spot");
    const rowNumber = spotElm.parentNode.getAttribute("row");
    const rowElm = spotElm.parentNode;
    const gridElm = rowElm.parentNode;
    const gridOwner = getIntFromString(gridElm.id);

    resolveSpot({
        spotNumber: spotNumber - [],
        spotElm: spotElm,
        rowNumber: rowNumber - [],
        rowElm: rowElm,
        gridElm: gridElm,
        gridOwner: gridOwner - []
    });

}

function resolveSpot(data) {
    game.lastResolvedSpot = game.current.spot ? game.current.spot : false;
    game.current.spot = data;
    let loc = game.current.spot;

    if (game.current.player === loc.gridOwner && game.current.roll > 0 && !game.grids[loc.gridOwner][loc.rowNumber][loc.spotNumber].roll) {
        placeDie(loc, game.current.roll);
    }
}

function placeDie(loc, roll) {
    game.current.dicePlaced = true;
    manElm(document.getElementById("roll"), { addClasss: "disabled", removeClass: "clickRoll" });
    manElm(document.getElementById("grid1"), { removeClass: "placeDice" });
    document.getElementById("specialRoll").classList.add("ninja");
    const dice = document.createElement("d" + roll);
    if (game.jokerRoll) {
        dice.classList.add("joker");
        game.jokerRoll = false;
        if (game.current.player === 1) {
            game.current.jokerPlays += 1;
            if (game.current.jokerPlays >= 3) {
                claimAchievement(12);
            }
        }
    }
    appendDots(dice, roll);
    loc.spotElm.innerHTML = "";
    loc.spotElm.appendChild(dice);
    dice.classList.add("animateDiceScale");
    setTimeout(() => { dice.classList.remove("animateDiceScale") }, game.ani.dice);
    game.current.roll = 0;
    game.grids[loc.gridOwner][loc.rowNumber][loc.spotNumber].roll = roll;
    updateColumData();
    resolveDice(loc, roll);

    manElm(document.getElementById("lastRoll"), { addClasss: "filterGray", removeClass: "animateDice" });
    manElm(document.getElementById("lastEnemyRoll"), { addClasss: "filterGray", removeClass: "animateDice" });
}

function resolveDice(loc, roll) {
    const info = game.current.info;
    let relevantColumn = info[loc.spotNumber];

    let who = "e";
    let whosGrid = 0;
    if (loc.gridOwner === 0) {
        who = "p";
        whosGrid = 1;
    }
    let removed = 0;
    if (relevantColumn[who].includes(roll)) {

        const boomChecker = (pos, gridRoot, rowNo, spotNo) => {
            if (pos && pos.roll === roll) {

                let boomElm = gridRoot.querySelectorAll("[row='" + rowNo + "'] [spot='" + spotNo + "']")[0];
                let gridOwner = getIntFromString(gridRoot.id);
                if (boomElm.childNodes.length > 0 && boomElm.childNodes[0].classList.contains("joker")) {
                    if (gridOwner == 0) { game.current.EJ += 1; }
                    else { game.current.J += 1; }
                }
                boomElm.classList.add("boom");
                setTimeout(() => {
                    boomElm.innerHTML = "";
                }, game.ani.dice);
                return true;
            };
            return false;
        };
        const gridRoot = document.getElementById("grid" + whosGrid);
        let pos0 = boomChecker(game.grids[whosGrid][0][loc.spotNumber], gridRoot, 0, loc.spotNumber);
        let pos1 = boomChecker(game.grids[whosGrid][1][loc.spotNumber], gridRoot, 1, loc.spotNumber);
        let pos2 = boomChecker(game.grids[whosGrid][2][loc.spotNumber], gridRoot, 2, loc.spotNumber);
        if (pos0) { game.grids[whosGrid][0][loc.spotNumber].roll = false; removed++; }
        if (pos1) { game.grids[whosGrid][1][loc.spotNumber].roll = false; removed++; }
        if (pos2) { game.grids[whosGrid][2][loc.spotNumber].roll = false; removed++; }
        if (who === "e") {
            if (removed === 3) { claimAchievement(10); }

        }
    }
    if (removed > 0) {
        game.current.rc++;
        if (game.current.player === 1) { game.current.prc++; }
        if (game.current.prc >= 5) { claimAchievement(9); }
    }
    else if (game.current.player === 1) {
        game.current.prc = 0;
    }
    else { game.current.rc = 0; }

    setTimeout(() => {
        cleanUpStep();
    }, game.ani.dice);


}
function upshiftRolls() {

    const upshift = (gridRoot, rowNo, spotNo, who) => {
        let currentSpot = gridRoot.querySelectorAll("[row='" + rowNo + "'] [spot='" + spotNo + "']");
        let nextSpot = gridRoot.querySelectorAll("[row='" + (who === "p" ? rowNo - 1 : rowNo + 1) + "'] [spot='" + spotNo + "']");
        if (nextSpot[0] && currentSpot[0]) {
            nextSpot[0].innerHTML = currentSpot[0].innerHTML;
            currentSpot[0].innerHTML = "";
        }

    };

    for (let g = 0; g < Object.keys(game.grids).length; g++) {
        let grid = game.grids[g];
        let who = "e";
        if (g === 1) { who = "p" }

        for (let i = 2; i >= 0; i--) {
            let row = grid[i];
            let prevGrid = (who === "p") ? grid[i - 1] : grid[i + 1];

            if (prevGrid) {
                if (row[0].roll && !prevGrid[0].roll) {
                    prevGrid[0].roll = row[0].roll;
                    row[0].roll = 0;
                    upshift(document.getElementById("grid" + g), i, 0, who)
                }

                if (row[1].roll && !prevGrid[1].roll) {
                    prevGrid[1].roll = row[1].roll;
                    row[1].roll = 0;
                    upshift(document.getElementById("grid" + g), i, 1, who)
                }

                if (row[2].roll && !prevGrid[2].roll) {
                    prevGrid[2].roll = row[2].roll;
                    row[2].roll = 0;
                    upshift(document.getElementById("grid" + g), i, 2, who)
                }
            }
        }
    }
}
function cleanUpStep() {
    document.querySelectorAll(".boom").forEach(boom => { boom.classList.remove("boom") });
    popup();
    upshiftRolls();
    upshiftRolls();
    calculatePoints();
    checkSequence(0);
    checkSequence(1);
    setTimeout(() => { nextTurn(); }, game.ani.aiTurnSpeed);
}
function calculatePoints() {
    updateColumData();
    let playerPoints = 0;
    let enemyPoints = 0;
    const info = game.current.info;

    const pointsFromArray = (arr) => {
        let points = 0;
        const count = numbers =>
            numbers.reduce((result, value) => ({
                ...result,
                [value]: (result[value] || 0) + 1
            }), {});
        const countArray = count(arr);

        for (let i = 1; i <= 6; i++) {
            if (countArray[i]) {
                points += countArray[i] > 1 ? countArray[i] * i * 2 : i
            }
        }
        return points;
    };

    playerPoints += pointsFromArray(info[0].p);
    enemyPoints += pointsFromArray(info[0].e);
    playerPoints += pointsFromArray(info[1].p);
    enemyPoints += pointsFromArray(info[1].e);
    playerPoints += pointsFromArray(info[2].p);
    enemyPoints += pointsFromArray(info[2].e);

    game.score.playerLast = game.score.player;
    game.score.enemyLast = game.score.enemy;
    game.score.player = playerPoints;
    game.score.enemy = enemyPoints;

    const scoreElm = document.getElementById("score");
    const enemyScoreElm = document.getElementById("enemyScore");

    if (game.score.playerLast > game.score.player) {
        scoreElm.classList.add("negative");
    }
    else if (game.score.playerLast < game.score.player) {
        scoreElm.classList.add("positive");
    }

    if (game.score.enemyLast > game.score.enemy) {
        enemyScoreElm.classList.add("negative");
    }
    else if (game.score.enemyLast < game.score.enemy) {
        enemyScoreElm.classList.add("positive");
    }

    setTimeout(() => {
        scoreElm.classList.remove("negative");
        scoreElm.classList.remove("positive");
        enemyScoreElm.classList.remove("negative");
        enemyScoreElm.classList.remove("positive");
    }, game.ani.def);

    enemyScoreElm.style.setProperty("--score", enemyPoints);
    scoreElm.style.setProperty("--score", playerPoints);
}
function checkSequence(gridNo) {

    const gridRoot = document.getElementById("grid" + gridNo);
    gridRoot.querySelectorAll("d1").forEach(d1 => {
        let d2 = nextDice(d1, 2);
        if (d2) {
            let d3 = nextDice(d2, 3);
            if (d3) {
                let d4 = nextDice(d3, 4);
                if (d4) {
                    let d5 = nextDice(d4, 5);
                    if (d5) {
                        let d6 = nextDice(d5, 6);
                        if (d6) {
                            d1.parentNode.classList.add("sequence");
                            d2.parentNode.classList.add("sequence");
                            d3.parentNode.classList.add("sequence");
                            d4.parentNode.classList.add("sequence");
                            d5.parentNode.classList.add("sequence");
                            d6.parentNode.classList.add("sequence");
                            game.score.sequence = gridNo === 1 ? "player" : "enemy";
                        }
                    }

                }

            }

        }

    });
    function nextDice(die, number) {
        let row = die.parentNode.parentNode;
        let nextDie = row.querySelectorAll("d" + number)[0];
        let nextSpot = die.parentNode.getAttribute("spot") - [];
        nextSpot = nextSpot > 1 ? 0 : nextSpot + 1;
        if (!nextDie) {
            let diceRowNo = die.parentNode.parentNode.getAttribute("row") - [];
            let nextRow = row.parentNode.querySelectorAll(".row[row='" + (diceRowNo + 1) + "']")[0];
            if (nextRow) {
                nextDie = nextRow.querySelectorAll("d" + number).length > 0 ? nextRow.querySelectorAll("d" + number)[0] : false;
            }

        }
        if (nextDie) {
            if (nextDie.parentNode.getAttribute("spot") - [] === nextSpot) {
                return nextDie;
            }
        }
        return false;

    }

}
function nextTurn() {

    if (checkForGameEnd()) {
        gameOver();
    }
    else {
        game.rollDisabled = false;
        if (game.current.player === 0) {
            game.current.player = 1;
            document.getElementById("roll").classList.remove("disabled");
            tutorial(game.current.player && !game.rollDisabled, ()=>{
                manElm(document.getElementById("roll"), { addClasss: "clickRoll" });
            })
        }
        else {
            game.current.turn += 1;
            game.current.player = 0;
            setTimeout(() => { aiTurn(); }, game.ani.aiTurnSpeed);

        }
    }
    document.getElementById("gTurn").style.setProperty("--score", (game.current.turn));


}

function randomFreeSpot(grid) {
    const row = grid[0];
    if (!row[1].roll) {
        return { spotNo: 1, rowNo: 0 }
    }
    else if (!row[0].roll) {
        return { spotNo: 0, rowNo: 0 }
    }

    else if (!row[2].roll) {
        return { spotNo: 2, rowNo: 0 }
    }
    gameOver();
}

function aiScan(simulateRoll, reverse) {
    updateColumData();
    let roll = reverse ? game.current.roll : game.current.ai.lastRoll;
    if (simulateRoll) { roll = simulateRoll }
    let e = "e";
    let p = "p";
    if (reverse) {
        e = "p";
        p = "e";
    }
    value = {
        0: 0, 1: 0, 2: 0
    };

    for (let i = 0; i < 3; i++) {
        let colum = game.current.info[i];
        value[i] -= (colum[e].length - 1) * 1.5;
        if (colum[e].length < 3) {
            if (colum[e].includes(roll)) {
                value[i] += 1;
                if (roll > 3) { value[i] += 0.5; }
                if (roll > 4) { value[i] += 1; }
                if (roll > 5) { value[i] += 0.5; }
                if (colum[e].join("") === roll + "" + roll) {
                    value[i] += 1;
                    if (game.current.ai.id === game.ais[1].id) {
                        value[i] += 2;
                    }
                }
                if (game.current.ai.id === game.ais[1].id) {
                    value[i] += 2;
                }
            }
            value['loc' + i] = {
                rowNo: 0,
                spotNo: i

            }
        }

        if (colum[p].includes(roll)) {
            value[i] += 1;
            if (game.current.ai.id === game.ais[2].id) {
                value[i] += 3;
            }
            if (colum[p].join("") === roll + "" + roll) {
                value[i] += 1;
            }
            if (roll > 2) { value[i] += 0.1; }
            if (roll > 3) { value[i] += 0.5; }
            if (roll > 4) { value[i] += 1; }
            if (roll > 5) { value[i] += 1; }
        }

    }

    return value;

}

function aiTurn(joker) {
    const AI = game.current.ai;
    let useJoker = false;
    roll(joker);

    AI.lastRoll = game.current.roll;
    const rollValue = aiScan();

    let RNGSpot = randomFreeSpot(game.grids[0]);


    let selectedSpot = RNGSpot;
    let spotValue = -1;
    if (rollValue.loc0 && rollValue[0] > spotValue) { selectedSpot = rollValue.loc0; spotValue = rollValue[0] }
    if (rollValue.loc1 && rollValue[1] > spotValue) { selectedSpot = rollValue.loc1; spotValue = rollValue[1] }
    if (rollValue.loc2 && rollValue[2] > spotValue) { selectedSpot = rollValue.loc2; spotValue = rollValue[2] }

    if (AI.id === game.ais[0].id) {
        if (rand(0, 10) > 2) {
            selectedSpot = RNGSpot;
        }
    }
    if (AI.id === game.ais[4].id && spotValue < 3 && AI.lastRoll < 5 && !joker && game.current.EJ > 0) {
        let eyeValue = {};
        let diceValueArray = [];
        for (let i = 1; i <= 6; i++) {
            eyeValue[i] = 0;
            let scan = aiScan(i);
            for (let s = 0; s < 3; s++) {
                eyeValue[i] += scan[s];
            }
            diceValueArray.push(eyeValue[i]);
        }
        const avr = diceValueArray.reduce((a, b) => a + b, 0) / diceValueArray.length;
        if (diceValueArray[AI.lastRoll - 1] < avr) {
            useJoker = true;
        }

    }
    if (AI.id === game.ais[3].id && spotValue < 2 && AI.lastRoll < 4 && !joker && game.current.EJ > 0) {
        useJoker = true;
    }


    if (useJoker) {
        setTimeout(() => { aiTurn(true); }, game.ani.def);
    }
    else {
        const gridRoot = document.getElementById("grid0");
        const spotNumber = selectedSpot.spotNo;
        const rowNumber = selectedSpot.rowNo;
        let spotElm = gridRoot.querySelectorAll("[row='" + rowNumber + "'] [spot='" + spotNumber + "']")[0];

        const loc = {
            spotNumber: spotNumber - [],
            spotElm: spotElm,
            rowNumber: rowNumber - [],
            gridElm: gridRoot,
            gridOwner: 0
        };

        setTimeout(() => {
            placeDie(loc, AI.lastRoll);
        }, game.ani.dice + 100);
    }



}


function checkForGameEnd() {
    updateColumData();
    freespots = 0;
    enemyFreeSpots = 0;
    const info = game.current.info;

    for (let i = 0; i < Object.keys(info).length; i++) {
        if (info[i].e.length < 3) { enemyFreeSpots++; }
        if (info[i].p.length < 3) { freespots++; }
    }

    if (freespots === 0 || enemyFreeSpots === 0 || game.score.sequence) {
        return true;
    }
    return false;
}
function gameOver() {
    const score = game.score.player;

    if (game.score.sequence) { claimAchievement(8) }
    if (score > game.score.enemy && game.score.sequence === "" || game.score.sequence === "player") {
        popup("You won!");
        game.current.ws += 1;
        claimAchievement(0);
        if (game.current.S) { claimAchievement(1) }
        if (game.current.ws === 5) { claimAchievement(2) }
        if ((score - 50) > game.score.enemy) { claimAchievement(13) }
        if (game.score.enemy === 0) { claimAchievement(11) }
        if (game.current.turn < 10) { claimAchievement(6) }
        if (game.current.turn > 30) { claimAchievement(7) }
    }
    else {
        game.current.ws = 0;
        popup(game.current.ai.id + " wins!");
    }

    if (game.current.hs < score) {
        game.current.hs = score;
    }

    setTimeout(() => { loadGame(); }, game.ani.ng)
}
function loadGame() {
    document.getElementById("highscore").style.setProperty("--score", game.current.hs);
    document.getElementById("winstreak").style.setProperty("--score", game.current.ws);
    makeAchievements();
    makeSkins();

    setTimeout(() => {
        resetBoard();
    }, game.ani.def);



}
function resetBoard() {
    document.querySelectorAll(".spot").forEach(spot => {
        spot.classList.add("boom");
        setTimeout(() => { spot.classList.remove("boom"); spot.classList.remove("sequence"); spot.innerHTML = "" }, game.ani.dice);
    });
    for (let g = 0; g < Object.keys(game.grids).length; g++) {
        for (let i = 0; i < Object.keys(game.grids[g]).length; i++) {
            let grid = game.grids[g];
            let row = grid[i];
            row[0].roll = false;
            row[1].roll = false;
            row[2].roll = false;
        }
    }
    document.getElementById("lastRoll").innerHTML = "";
    document.getElementById("score").style.setProperty("--score", 0);
    game.current.turn = 0;
    game.current.rc = 0;
    game.current.playerRoll = 0;
    game.current.jokerPlays = 0;
    game.current.J = 1;
    game.current.EJ = 1;
    game.score.sequence = "";
    updateColumData();
    loadAi(game.current.ws > 4 ? 0 : game.current.ws);

    setTimeout(() => {
        game.current.player = rand(0, 1);
        if (game.current.ws === 0) { game.current.player = 0; }
        game.rollDisabled = false;
        document.getElementById("grid1").className = "grid " + game.S[game.current.S];
        document.getElementsByClassName("refBar")[1].className = "refBar flex " + game.S[game.current.S];
        if (game.current.player) {
            popup("Player goes first.");
            document.getElementById("roll").classList.remove("disabled");
        }
        else {
            popup(game.current.ai.id + " goes first.");
            setTimeout(() => { aiTurn(); }, game.ani.def);
        }
    }, game.ani.def);
}
function claimAchievement(id) {
    swapView('game');
    const root = document.getElementById("ax" + id);
    if (game.achievements[id].l === 0) {
        root.classList.add("yai");
        setTimeout(() => { root.classList.remove("yai"); }, game.ani.def * 3);
    }
    game.achievements[id].l = 1;
    root.classList.remove("locked");

    let achievementCount = 0;
    for (let i = 0; i < Object.keys(game.achievements).length; i++) {
        if (game.achievements[i].l) {
            achievementCount++;
        }
    }
    if (achievementCount === Object.keys(game.achievements).length - 1) {
        claimAchievement(14)
    }


}

function makeAchievements() {
    const root = document.getElementById("rewards");
    root.innerHTML = "";
    for (let i = 0; i < Object.keys(game.achievements).length; i++) {
        let a = game.achievements[i];
        let div = document.createElement("div");
        div.title = a.t;
        div.id = "ax" + i;
        div.innerHTML = a.n;
        div.className = "achievements " + (a.l ? "" : "locked");
        root.appendChild(div);

    }
}
function popup(content) {
    const pop = document.getElementById("popups");
    if (!content) { pop.classList.add("ninja") }
    else {
        pop.innerHTML = "<div>" + content + "</div>";
        pop.onclick = () => { pop.classList.add("ninja") };
        pop.classList.remove("ninja");
    }
}
function appendDots(elm, die) {
    for (let i = 0; i < die; i++) {
        elm.appendChild(document.createElement("dot"));
    }

}
function swapView(id) {
    clearAllViews();
    document.getElementById(id).classList.remove("ninja");
}
function clearAllViews() {
    document.getElementById("rules").classList.add("ninja");
    document.getElementById("game").classList.add("ninja");
    document.getElementById("settings").classList.add("ninja");
}

function updateColumData() {
    let columns = {
        0: { p: [], e: [] }, 1: { p: [], e: [] }, 2: { p: [], e: [] }
    };
    for (let g = 0; g < Object.keys(game.grids).length; g++) {
        let grid = game.grids[g];
        let who = "e";
        if (g === 1) { who = "p" }

        for (let i = 0; i < Object.keys(grid).length; i++) {
            let row = grid[i];

            if (row[0].roll) { columns[0][who].push(row[0].roll) }
            if (row[1].roll) { columns[1][who].push(row[1].roll) }
            if (row[2].roll) { columns[2][who].push(row[2].roll) }
        }
    };
    for (let i = 0; i < 3; i++) {
        if (columns[i].p.join("") === "111") { claimAchievement(4); }
        if (columns[i].p.join("") === "666") { claimAchievement(5); }
    }

    game.current.info = columns;

}
function boot() {
    const root = document.getElementById("gameboard");
    clearAllViews();
    for (let i = 0; i < game.gridCount; i++) {
        let grid = buildGrid();
        game.grids[i] = grid.gridObj;
        grid.root.id = "grid" + i;
        document.getElementById("gameboard").appendChild(grid.root);
    }
    document.querySelectorAll("d6").forEach(elm => {
        appendDots(elm, 6);
    });
    const ani = game.ani.def - [];
    game.ani.def = 1;
    loadGame();
    makeSkins();
    game.ani.def = ani;



    document.addEventListener("keyup", (event) => {
        const key = event.code;
        const controls = game.controls;
        if (controls.roll === key || controls[1] === key || controls[2] === key || controls[3] === key) {
            event.stopPropagation();
            event.preventDefault();
            let grid = game.grids[game.current.player];
            if (key === controls.roll && (game.current.player === 1)) {
                roll(game.rollDisabled ? true : false);
            }
            else if (key === controls[1]) {
                if (!grid[2][0].roll) {
                    spotclick(document.querySelectorAll("#grid" + [game.current.player] + " div[row='2'] div[spot='0']")[0]);
                }
            }
            else if (key === controls[2]) {
                if (!grid[2][1].roll) {
                    spotclick(document.querySelectorAll("#grid" + [game.current.player] + " div[row='2'] div[spot='1']")[0]);
                }
            }
            else if (key === controls[3]) {
                if (!grid[2][2].roll) {
                    spotclick(document.querySelectorAll("#grid" + [game.current.player] + " div[row='2'] div[spot='2']")[0]);
                }
            }
        }

        else if (game.controls.newMapping) {
            let binding = game.controls.btn.getAttribute("binding");
            if (binding !== "roll") {
                binding = binding - [];
            }
            game.controls[binding] = key;
            game.controls.btn.value = key;
            game.controls.newMapping = false;
        }
    });

    document.querySelectorAll("#settings input[type='button']").forEach(btn => {
        btn.addEventListener("click", () => {
            game.controls.newMapping = true;
            game.controls.btn = btn;
            btn.value = "Press a new key for this action.";
        });
    });

    swapView('rules');
}
function loadAi(aiLvl) {

    game.current.ai = game.ais[aiLvl];
    document.getElementById("aiName").style.setProperty("--score", (aiLvl + 1));
    document.getElementById("lastEnemyRoll").innerHTML = "";
    document.getElementById("enemyScore").style.setProperty("--score", 0);
    setTimeout(() => {
        document.getElementById("grid0").className = "grid " + game.S[game.current.ai.S];
        document.getElementsByClassName("refBar")[0].className = "refBar flex " + game.S[game.current.ai.S];
    }, game.ani.def);

}
function makeSkins() {
    const root = document.getElementById("skins");
    root.innerHTML = "";
    for (let i = 0; i < Object.keys(game.S).length; i++) {
        let skin = manElm(false, {
            addClasss: game.S[i],
            inner: "<d6></d6>",
        });
        root.appendChild(skin);
        appendDots(skin.getElementsByTagName("d6")[0], 6);
        if (i === game.current.S) { skin.classList.add("selected") }
        if (i == 2 && !game.achievements[5].l) {
            skin.classList.add("locked");
        }
        else if (i == 3 && !game.achievements[0].l) {
            skin.classList.add("locked");
        }
        else if (i == 4 && !game.achievements[2].l) {
            skin.classList.add("locked");
        }
        else if (i == 5 && !game.achievements[14].l) {
            skin.classList.add("locked");
        }
        else {
            skin.onclick = (e) => {
                game.current.S = i;
                root.childNodes.forEach(s => {
                    s.classList.remove("selected");
                });
                e.currentTarget.classList.add("selected");
                document.getElementById("grid1").className = "grid " + game.S[game.current.S];
                document.getElementsByClassName("refBar")[1].className = "refBar flex " + game.S[game.current.S];
            }
        }

    }

}

setTimeout(boot());

/*
Achievemnt to be reworked 
Add Skin - Swap a skin 
- Win Streak
- Only Aces 
- Feeling blue 
*/